# ttn-source worker

For the TTN Source Worker to function a few environment variables need to be set:

- WORKER_HTTP_PORT, the port on which the HTTP Server should be available
- WORKER_ZMQ_ENDPOINT, the ZMQ host which will receive the processed messages from the TTN Source Worker