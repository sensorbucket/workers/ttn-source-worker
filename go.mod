module sensorbucket.nl/workers/ttn-source-worker

go 1.18

require (
	github.com/sirupsen/logrus v1.9.0
	github.com/zeromq/goczmq v4.1.0+incompatible
)

require github.com/hashicorp/errwrap v1.0.0 // indirect

require (
	github.com/hashicorp/go-multierror v1.1.1
	github.com/pebbe/zmq4 v1.2.8
	github.com/rabbitmq/amqp091-go v1.5.0
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
