package main

import (
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"sensorbucket.nl/sensorbucket/pkg/mq"
	"sensorbucket.nl/workers/ttn-source-worker/internal/device_service"
	"sensorbucket.nl/workers/ttn-source-worker/internal/handler"

	"github.com/rabbitmq/amqp091-go"
	"github.com/sirupsen/logrus"
)

var (
	DEVICE_SVC_EP = mustBeDefined("DEVICE_SERVICE_ENDPOINT")
	AMQP_QUEUE    = mustBeDefined("AMQP_QUEUE")
	AMQP_URL      = mustBeDefined("AMQP_URL")
	AMQP_XCHG     = mustBeDefined("AMQP_XCHG")
	AMQP_PREFETCH = mustBeDefined("AMQP_PREFETCH")
)

func main() {
	logger := logrus.WithFields(logrus.Fields{
		"queue": AMQP_QUEUE,
		"host":  AMQP_URL,
		"xchg":  AMQP_XCHG,
	})
	prefetch, err := strconv.Atoi(AMQP_PREFETCH)
	if err != nil {
		logger.WithError(err).Error("Prefetch is not an integer")
		return
	}
	logger.Info("starting ttn source worker")
	producer := mq.NewAMQPPublisher(AMQP_URL, AMQP_XCHG, func(c *amqp091.Channel) error {
		return c.ExchangeDeclare(AMQP_XCHG, "topic", true, false, false, false, nil)
	})
	go producer.Start()

	consumer := mq.NewAMQPConsumer(AMQP_URL, AMQP_QUEUE, func(c *amqp091.Channel) error {
		_, err := c.QueueDeclare(AMQP_QUEUE, true, false, false, false, amqp091.Table{})
		c.Qos(prefetch, 0, true)
		return err
	})
	go consumer.Start()

	h := handler.New(
		device_service.DeviceService{
			Endpoint: DEVICE_SVC_EP,
		},
	)
	go func(h *handler.Handler) {
		for {
			msg := <-consumer.Consume()
			topic, b, err := h.Handle(msg.Body)
			if err != nil {
				logrus.WithError(err).Error("erred while processing pipeline model")
				msg.Ack(false)
				continue
			}
			err = producer.Publish(topic, amqp091.Publishing{
				Body: b,
			})
			if err != nil {
				logrus.WithField("data", string(b)).WithError(err).Error("erred while publishing measurement to mq")
			}
			msg.Ack(false)
		}
	}(h)
	logger.Info("started ttn source worker")

	// wait for a signal to shutdown
	sigC := make(chan os.Signal, 1)
	signal.Notify(sigC, syscall.SIGINT, syscall.SIGTERM)
	<-sigC
	consumer.Shutdown()
	producer.Shutdown()
	logrus.Info("shutting down")
}

func mustBeDefined(envVar string) string {
	val := os.Getenv(envVar)
	if val == "" {
		panic(fmt.Sprintf("environment variable '%s' not defined", envVar))
	}
	return val
}
