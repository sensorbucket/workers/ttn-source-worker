package handler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"sensorbucket.nl/sensorbucket/pkg/pipeline"
	"sensorbucket.nl/workers/ttn-source-worker/internal/device_service"
	"sensorbucket.nl/workers/ttn-source-worker/internal/models"
)

type Handler struct {
	deviceService device_service.DeviceService
}

func New(deviceService device_service.DeviceService) *Handler {
	return &Handler{deviceService}
}

// returns routing key, data to push and an error if present
func (h *Handler) Handle(msg []byte) (string, []byte, error) {
	var pipelineMsg pipeline.Message
	if err := json.Unmarshal(msg, &pipelineMsg); err != nil {
		logrus.WithError(err).Warn()
		return "", nil, fmt.Errorf("received invalid pipeline message model, err: %s", err)
	}
	var ttnMsg models.TTNMessage
	if err := json.Unmarshal(pipelineMsg.Payload, &ttnMsg); err != nil {
		return "", nil, fmt.Errorf("received invalid ttn message on pipeline model, err: %s", err)
	}
	meas, err := ttnMsg.ToMeasurements()
	if err != nil {
		// don't return because not all measurements may have failed converting
		logrus.WithError(err).Warn("erred while converting ttn message to measurements")
	}

	device, err := h.deviceService.GetDeviceByEui(ttnMsg.EndDeviceId.EUI)
	if err != nil {
		return "", nil, err
	}

	if len(pipelineMsg.PipelineSteps) == 0 {
		return "", nil, fmt.Errorf("pipelinesteps were empty, don't know where to push message")
	}

	popped, err := pipelineMsg.NextStep()
	if err != nil {
		logrus.Warn("erred while popping")
	}
	pipelineMsg.Device = &device
	pipelineMsg.Measurements = meas
	pipelineMsg.Payload = ttnMsg.MeasurementPayload()

	timestamp, err := time.Parse(time.RFC3339, ttnMsg.Timestamp)
	if err != nil {
		return "", nil, err
	}
	pipelineMsg.Timestamp = timestamp.UnixMilli()

	data, err := json.Marshal(pipelineMsg)
	if err != nil {
		logrus.WithError(err).Info()
		return "", nil, fmt.Errorf("erred while marshalling pipeline message, err: %s", err)
	}
	return popped, data, nil
}
