package device_service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"sensorbucket.nl/sensorbucket/pkg/pipeline"
)

type DeviceService struct {
	Endpoint string
}

type DeviceDTO struct {
	Data []pipeline.Device `json:"data"`
}

func (ds *DeviceService) GetDeviceByEui(eui string) (pipeline.Device, error) {
	type config struct {
		EUI string `json:"eui,omitempty"`
	}
	if eui == "" {
		return pipeline.Device{}, fmt.Errorf("cannot retrieve device, eui is empty")
	}
	cfg := config{
		EUI: eui,
	}
	b, err := json.Marshal(cfg)
	if err != nil {
		return pipeline.Device{}, err
	}
	ep := fmt.Sprintf("%s/devices?configuration=%s", ds.Endpoint, url.QueryEscape(string(b)))
	res, err := http.Get(ep)
	if err != nil {
		return pipeline.Device{}, err
	}
	var response DeviceDTO
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return pipeline.Device{}, err
	}
	devices := response.Data
	if len(devices) != 1 {
		return pipeline.Device{}, fmt.Errorf("expected 1 device but got %d", len(devices))
	}
	return devices[0], nil
}
