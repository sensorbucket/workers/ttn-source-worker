package models

import (
	"fmt"
	"time"

	"github.com/hashicorp/go-multierror"
	"sensorbucket.nl/sensorbucket/pkg/pipeline"
)

const (
	RSSI = "rssi"
	SNR  = "snr"
)

// The model as we expect it from TTN
type TTNMessage struct {
	Timestamp string `json:"received_at"`
	Uplink    struct {
		FrmPayload []byte `json:"frm_payload,omitempty"`
		RxMetaData []struct {
			GatewayId struct {
				EUI       string `json:"eui,omitempty"`
				GatewayId string `json:"gateway_id,omitempty"`
			} `json:"gateway_ids,omitempty"`
			Timestamp string  `json:"time,omitempty"`
			SNR       float64 `json:"snr,omitempty"`
			RSSI      float64 `json:"rssi,omitempty"`
		} `json:"rx_metadata,omitempty"`
	} `json:"uplink_message,omitempty"`
	EndDeviceId struct {
		EUI string `json:"dev_eui"`
	} `json:"end_device_ids"`
}

type Device struct {
	EUI string `json:"eui"`
}

func (ttnMsg *TTNMessage) MeasurementPayload() []byte {
	return ttnMsg.Uplink.FrmPayload
}

func (ttnMsg *TTNMessage) ToMeasurements() ([]pipeline.Measurement, error) {
	measurements := []pipeline.Measurement{}
	var err error
	for _, gw := range ttnMsg.Uplink.RxMetaData {
		timestamp, err := time.Parse(time.RFC3339, gw.Timestamp)
		if err != nil {
			err = multierror.Append(fmt.Errorf("invalid timestamp, err: %s", err))
		}
		measurements = append(measurements, pipeline.Measurement{
			Timestamp:  timestamp.UnixMilli(),
			Value:      gw.RSSI,
			SensorCode: nil,
			Metadata: map[string]any{
				"gw_id":  gw.GatewayId.GatewayId,
				"gw_eui": gw.GatewayId.EUI,
			},
			MeasurementTypeID: RSSI,
		})
		measurements = append(measurements, pipeline.Measurement{
			Timestamp:  timestamp.UnixMilli(),
			Value:      gw.SNR,
			SensorCode: nil,
			Metadata: map[string]any{
				"gw_id":  gw.GatewayId.GatewayId,
				"gw_eui": gw.GatewayId.EUI,
			},
			MeasurementTypeID: SNR,
		})
	}
	return measurements, err
}
