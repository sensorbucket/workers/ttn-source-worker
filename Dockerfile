FROM golang:1.18-alpine AS builder

WORKDIR /app

COPY go.sum .
COPY go.mod .
RUN go mod download

RUN apk update && apk add --no-cache libzmq zeromq-dev pkgconfig git make build-base && rm -rf /var/cache/apk/*

COPY . .
RUN go build -o ttn-source-worker cmd/ttn-source-worker/main.go


FROM alpine AS final
RUN apk update && apk add --no-cache libzmq zeromq-dev && rm -rf /var/cache/apk/*
COPY --from=builder /app/ttn-source-worker /ttn-source-worker
ENTRYPOINT [ "/ttn-source-worker" ]